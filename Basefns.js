var Basefns = function () {

    this.clickElement = async function (elm) {
        try {
            //console.log(await browser.isElementPresent(elm)+" Element is Present")
            expect(await browser.isElementPresent(elm)).toBe(true, await elm.getText() + " is Present");
            console.log(await elm.getText() + " Element's Innertext is displaying")
            await browser.executeScript("arguments[0].scrollIntoView();", elm);
            await elm.click()
            browser.sleep(2000)
        } catch (e) {
            fail(e);
        }
    };

    this.ElementStyleValidation = async function (elm, ele_Name, ExpStyle) {
        try {

            //Comp radio button visual validations

            console.log(ele_Name + " is present: " + await browser.isElementPresent(elm))

            //background color
            console.log(ele_Name + " background color: " + await elm.getCssValue('background-color'))
            expect(await elm.getCssValue('background-color')).toBe(ExpStyle['background-color'], ele_Name + " background color is displaying as expected")

            //text color
            console.log(ele_Name + " text color: " + await elm.getCssValue('color'))
            expect(await elm.getCssValue('color')).toBe(ExpStyle['font-color'], ele_Name + " text color is displaying as expected")

            //font family
            console.log(ele_Name + " font-family: " + await elm.getCssValue('font-family'))
            expect(await elm.getCssValue('font-family')).toBe(ExpStyle['font-family'], ele_Name + " font-family is displaying as expected")

            //font size
            console.log(ele_Name + " font-size: " + await elm.getCssValue('font-size'))
            expect(await elm.getCssValue('font-size')).toBe(ExpStyle['font-size'], ele_Name + " font-size is displaying as expected")

            //text-align
            console.log(ele_Name + " text-align: " + await elm.getCssValue('text-align'))
            expect(await elm.getCssValue('text-align')).toBe(ExpStyle['text-align'], ele_Name + " text-align is displaying as expected")

            //padding
            console.log(ele_Name + " padding: " + await elm.getCssValue('padding'))
            expect(await elm.getCssValue('padding')).toBe(ExpStyle['padding'], ele_Name + " padding is displaying as expected")
            console.log()
        } catch (e) {
            fail(e);
        }
    };
}
module.exports = new Basefns();
Steps to execute the ShadowDom Demo automation
    1. Check Node.js is installed in the your local machine and it is added to the System Environment path. if it is not. please download the new version of Node.js and install in your system and add it to local machine system environment path.
       * Syntax to check the node.js and its availability in local machine. Enter "node -v" to check node version and its availability. 
       * Search for Environment variable in the start menu and select "Edit System Environment variable" => In System properties, Click on "Environment Variable" button under Advance tab => In Environment variable pop up. you will find System variable section, Select "path" the list and click Edit => In "Edit Environment Variable" pop up, Check "C:\Program Files\nodejs\node_modules\npm\bin" is available or not. if it is not please click on new and add it to the path.

    2. Install protractor and Webdriver update globally in your local machine using the below commands in CMD
        *npm install -g protractor
        *webdriver-manager update

    3. Open Automation in "Visual Studio code", Clone the project from "Git lab" or UnZib the project and open the project in Visual Studio code.

    4. Open Terminal, run "npm install" and "npm run wd-update". After the node_modules folder is downloaded. Project is good to execute.

    5. Use "npm run test" to execute test case.

    6. In case, if any issue releated to Session browser compatibility. check the local machine browser verions and Driver version is same. if not please update accordingly.

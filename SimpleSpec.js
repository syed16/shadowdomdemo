var obase = require('./Basefns.js');


describe('protractor Demo', function () {
    it('Validating Comp radio button and Button 1 css styles', async function () {
        try {
            //Launching Browser
            browser.ignoreSynchronization = true;
            browser.manage().timeouts().pageLoadTimeout(400000);
            browser.manage().timeouts().implicitlyWait(25000);
            browser.driver.manage().window().maximize();
            await browser.get('http://www.edisondesignsystem.com/');
            var pageTitle = await browser.getTitle();
            expect(pageTitle).toEqual('EDISON DESIGN SYSTEM');
            browser.sleep(5000)
            //Shadow root of app
            var sh1 = browser.findElement(by.css('eds-docs-app'));
            var sr1 = await findShadowDomElement(sh1)
            //Side Panel Shadow root
            var sh2 = sr1.findElement(by.css('eds-sidenav'));
            var sr2 = await findShadowDomElement(sh2)

            //Clicking on component option from side panel
            var Components = sr2.findElement(by.css("button[aria-controls='main7']"))
            await obase.clickElement(Components);

            //Clicking on Btton options from component section
            var btn_comp = sr2.findElement(by.css("button[aria-controls='menu4']"))
            await obase.clickElement(btn_comp);

            //Main screen Shadow root
            var sh_btnGrp = sr1.findElement(by.css("eds-button-group-demo"));
            var sr_btnGrp = await findShadowDomElement(sh_btnGrp)

            var sh_docPg = sr_btnGrp.findElement(by.css("eds-docs-page-ng"));
            var sr_docPg = await findShadowDomElement(sh_docPg)

            //Elemets tabs shadow root
            var sh_tabs = sr_docPg.findElement(by.css("eds-tabs"));
            var sr_tabs = await findShadowDomElement(sh_tabs)

            //Selecting Css Only code Tab 
            var css_onlyTabs = sr_tabs.findElement(by.css("button#css"));
            await obase.clickElement(css_onlyTabs)

            //Sample componets section shadow root
            var sh_btnEx = sr_docPg.findElement(by.css("eds-docs-example-ng"));
            var sr_btnEx = await findShadowDomElement(sh_btnEx)

            //Component porperty pannel shadow root
            var sh_docPpb = sr_btnEx.findElement(by.css("eds-docs-property-block-ng"));
            var sr_docPpb = await findShadowDomElement(sh_docPpb)

            //Comp radio option button style validation
            console.log("\nComp Radio button <=====> style validation result")
            var opComp = sr_docPpb.findElement(by.css("label[for='compOption']"))
            var Comp_option_Style_Formate = {
                "background-color": "rgba(99, 102, 106, 1)",
                "font-color": "rgba(250, 251, 253, 1)",
                "font-family": "\"GE Inspira Sans\", ge-inspira-sans, ge-sans, sans-serif",
                "font-size": "16px",
                "text-align": "center",
                "padding": "7px 24px"
            }
            await obase.ElementStyleValidation(opComp, "Comp radio button", Comp_option_Style_Formate)// Function to validate all Css styling
            
            //Button examples viewer shadow root
            var sh_docViw = sr_btnEx.findElement(by.css("eds-docs-example-viewer-ng"))
            var sr_docViw = await findShadowDomElement(sh_docViw)

            //Button 1 Style validation before selecting
            console.log("Button1 before selection <=====> style validation result")
            var button1 = sr_docViw.findElement(by.css("div#componentSlot > div > label[for=id1]"))
            console.log(await button1.getText() + " is present: " + await browser.isElementPresent(button1))
            var btn1_Style_Formate = {
                "background-color": "rgba(236, 236, 240, 1)",
                "font-color": "rgba(43, 45, 46, 1)",
                "font-family": "\"GE Inspira Sans\", ge-inspira-sans, ge-sans, sans-serif",
                "font-size": "16px",
                "text-align": "center",
                "padding": "7px 24px"
            }
            await obase.ElementStyleValidation(button1, "Button 1", btn1_Style_Formate)

            //Validating the button 1 after clicking, Expected background color and font color change.
            console.log("Button1 after selection <=====> style validation result")
            await obase.clickElement(button1);
            browser.sleep(2000)
            var btn1_sel_Style_Formate = {
                "background-color": "rgba(99, 102, 106, 1)",
                "font-color": "rgba(250, 251, 253, 1)",
                "font-family": "\"GE Inspira Sans\", ge-inspira-sans, ge-sans, sans-serif",
                "font-size": "16px",
                "text-align": "center",
                "padding": "7px 24px"
            }
            await obase.ElementStyleValidation(button1, "Button 1 after Selection", btn1_sel_Style_Formate)

            browser.sleep(3000)
        } catch (err) {
            fail("Error: " + err)
        }


    });
    async function findShadowDomElement(shadowHost) {
        try {
            return await browser.executeScript("return arguments[0].shadowRoot", shadowHost);
        } catch (err) {
            fail("Error: " + err)
        }
    }

});

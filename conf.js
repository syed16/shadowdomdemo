// conf.js

exports.config = {
    framework: 'jasmine',
    jasmineNodeOpts: { defaultTimeoutInterval: 260000 },
    specs: ['simpleSpec.js'],
    directConnect: true,
    capabilities: {
        browserName: 'chrome',
    },
    // need to install jasmine repot globally in CMD with the following command npm install -g jasmine-reporters
    onPrepare: function () { //cofigure junit xml report

        let HtmlReporter = require('protractor-beautiful-reporter');
        jasmine.getEnv().addReporter(new HtmlReporter({
            baseDirectory: 'reports_new',
            screenshotsSubfolder: 'screenshots',
            // takeScreenShotsOnlyForFailedSpecs: true,
            jsonsSubfolder: 'jsonFiles',
            excludeSkippedSpecs: true,
            preserveDirectory: false,
            clientDefaults: {
                showTotalDurationIn: "header",
                totalDurationFormat: "h:m:s",
                gatherBrowserLogs: true
            },
        }).getJasmine2Reporter());
    }
}